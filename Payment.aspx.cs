﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GarantiOdeme("234523453245", "100");
        }
        protected void GarantiOdeme(string SNo, string OdenecekTutar)
        {

            string strMode = "TEST";
            string strApiVersion = "v0.00";
            string strTerminalProvUserID = "PROVAUT";
            string strType = "sales";
            string strAmount = OdenecekTutar; //İşlem Tutarı
            string strCurrencyCode = "949";
            string strInstallmentCount = "1"; //Taksit Sayısı. Boş gönderilirse taksit yapılmaz
            string strTerminalUserID = "12345";
            string strOrderID = SNo;
            string strCustomeripaddress = "";
            string ipp = "";
            try
            {
                ipp = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            catch { }
            if (ipp == "")
            {
                ipp = "1.1.1.1";
            }
            strCustomeripaddress = ipp;
            string strcustomeremailaddress = "shital.nagare@polymerupdate.com";
            string strTerminalID = "30691300";
            string _strTerminalID = "0" + strTerminalID; //Başına 0 eklenerek 9 digite tamamlanmalıdır.
            string strTerminalMerchantID = "7000679"; //Üye İşyeri Numarası
            string strStoreKey = "12345678"; //3D Secure şifresi
            string strProvisionPassword = "123qweASD"; //Terminal UserID şifresi
            string strSuccessURL = "http://localhost:64207/PayResult.aspx";
            string strErrorURL = "http://localhost:64207/PayResult.aspx";
            string strcompanyName = "TestCompany";
            string strLang = "en";
            string strrefreshTime = "10";
            string strTimestamp = SNo; //Random ve Unique bir değer olmalı
            string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
            string HashData = GetSHA1(strTerminalID + strOrderID + strAmount + strSuccessURL + strErrorURL + strType + strInstallmentCount + strStoreKey + SecurityData).ToUpper();



            NameValueCollection Data = new NameValueCollection();
            Data.Add("mode", strMode);
            Data.Add("secure3dsecuritylevel", "3D");
            Data.Add("apiversion", strApiVersion);
            Data.Add("terminalprovuserid", strTerminalProvUserID);
            Data.Add("terminaluserid", strTerminalUserID);
            Data.Add("terminalmerchantid", strTerminalMerchantID);
            Data.Add("txntype", strType);
            Data.Add("txnamount", strAmount);
            Data.Add("txncurrencycode", strCurrencyCode);
            Data.Add("txninstallmentcount", strInstallmentCount);
            Data.Add("orderid", strOrderID);
            Data.Add("terminalid", strTerminalID);
            Data.Add("successurl", strSuccessURL);
            Data.Add("errorurl", strErrorURL);
            Data.Add("customeremailaddress", strcustomeremailaddress);
            Data.Add("customeripaddress", strCustomeripaddress);
            Data.Add("secure3dhash", HashData);
            Data.Add("strCompanyName", strcompanyName);
            Data.Add("strlang", strLang);
            Data.Add("strRefreshTime", strrefreshTime);
            Data.Add("strtimestamp", strTimestamp);
            Data.Add("cardnumber", cardnumber.Text);
            Data.Add("cardexpiredatemonth", cardexpiredatemonth.Text);
            Data.Add("cardexpiredateyear", cardexpiredateyear.Text);
            Data.Add("cardcvv2", cardcvv2.Text);

            RedirectAndPOST(this.Page, "https://sanalposprovtest.garanti.com.tr/VPServlet", Data);
        }
        public static void RedirectAndPOST(Page page, string destinationUrl, NameValueCollection data)
        {
            string strForm = PreparePOSTForm(destinationUrl, data);
            page.Controls.Add(new LiteralControl(strForm));
        }
        private static String PreparePOSTForm(string url, NameValueCollection data)
        {
            string formID = "PostForm";
            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" +
                           formID + "\" action=\"" + url +
                           "\" method=\"POST\">");

            foreach (string key in data)
            {
                strForm.Append("<input type=\"hidden\" name=\"" + key +
                               "\" value=\"" + data[key] + "\">");
            }

            strForm.Append("</form>");
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language=\"javascript\">");
            strScript.Append("var v" + formID + " = document." +
                             formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");
            return strForm.ToString() + strScript.ToString();
        }
        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }
        public string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }
    }
}