﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="WebApplication1.Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table border="0" cellspacing="4" cellpadding="4">
            <tr>
                <td width="30%">Kart No</strong>
                </td>
                <td width="5%">
                    <strong>:</strong>
                </td>
                <td width="65%">
                    <asp:TextBox ID="cardnumber" MaxLength="16" Font-Size="16px" Width="180px" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Son Kullanma Tarihi </strong>
                </td>
                <td>
                    <strong>:</strong>
                </td>
                <td>
                    <div style="width: 40px; float: left; margin-right: 15px;">
                        <asp:TextBox ID="cardexpiredatemonth" MaxLength="2" Font-Size="16px" Width="40px"
                            runat="server" />
                    </div>
                    <div style="width: 40px; float: left;">
                        <asp:TextBox ID="cardexpiredateyear" MaxLength="2" Font-Size="16px" Width="40px"
                            runat="server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>Cvc2
                </td>
                <td>
                    <strong>:</strong>
                </td>
                <td>
                    <asp:TextBox ID="cardcvv2" MaxLength="3" Font-Size="16px" Width="40px" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button ID="Button1" runat="server" Text="Öde" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
